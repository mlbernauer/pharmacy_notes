# PHRM 752 - Epilepsy I

_by_ [_Michael L. Bernauer_](http://mlbernauer.bitbucket.org/)

## Learning Objectives
1. Discuss the epidemiology and basic pathophysiology of epilepsy.
2. Discuss and compare the mechanisms of action and adverse reactions of 
   antiepileptic drugs.
3. Compare the clinical presentation of partial seizures with generalized
   seizures.
4. Create general treatment goals for a patient with a seizure disorder.
5. Identify and evaluate factors that would guide selection of a specific 
   antiepileptic drug for an individual patient.
6. Analyze published evidence-based revie for the treatment of epilepsy and
   select an appropriate antiepileptic drug for a patient based on patient-specific
   factors.
7. Create appropriate medication management of epilepsy based on hormonal/reproductive
   health considerations of an antiepileptic drug treatment.
8. Formulate a monitoring plan for a patient on a given antiepileptic drug based
   on patient-specific factors.
9. Identify nonpharmacologic therapy for patients with refractory seizures.
10. Discuss the issues regarding antiepileptic drugs product bioequivalence and
    advantages of maintaining patients on the same product.

## Notes
### Causes of seizures
Seizures can be idiopathic/geneitic, cryptogenic (unknown cause) or the result of
neurodegenerative disorders (Alzheimers), result from electrolyte abnormality, TBI,
brain tumor or brain surgery.

### Triggers
Seizures can be triggered through medications (stimulants, GABA inhibitors, antibiotics),
menstrual cycels, anxiety and sleep deprivation.

### Diagnosis
When diagnosing epilepsy it is important to ascertain _duration, frequency, description, post-ictal symptoms (headahce, fatigue)_. Genetic testing may be useful (Dravet syndrome-Na channel). Medication history is also important; for example which medications have been tried to
treat seizure, which succeeded/failed.

EEG can be useful in determining the location of origin of the seizure, radiology
(CT/MRI, fMRI) can help identify structural abnormalities).

### Classification
* Partial
    * Simple partial (SPS) - consciousness preserved with or w/o motor symptoms.
    * Complex partial (CPS) - consciousness not preserved, with or w/o motor symptoms.
    * Secondarily generalized - follows SPS or CSP both hemispheres with or w/o moto                                         symptoms.
* Generalized
    * Absence - staring spells (20-30 sec) with sudden resolution.
    * Myoclonic - brief shock-like jerk of muscle/muscle group.
    * Atonic - symmetric, tonic muscle contraction of extremities, waist, neck.
    * Tonic - sudden loss of postural tone.
    * Tonic-clonic - stifening and fall with rhythmic extremity jerking.

### Treatment goals
The ultimate goal of treatment is life-long seizure free status. However, well-controlled
seizure status with minimal adverse outcomes from treatment with minimal adverse events (AEs) from antiepileptic drugs (AEDs) is satisfactory.

### AED classification
AEDs are classified based on whether they are newer or older agents, enzyme inducing and pharmacologic classification.
* Mechanisms of action:
    * Na<sup>+</sup> channel blocker
    * Ca<sup>2+</sup> channel blcoker
    * GABA enhancer
    * K channel agonist
    * AMPA receptor antagonist
    * NMDA receptor antagonist 
* Older agents:
    * Phenobarbitol
    * Phenytoin
    * Primidone
    * Ethosuximide - absence seizures
    * Carbamezapine
    * Valproic acid  - absence seizures
    * Divalproex Na - absence seizures
* Newer agents:
    * Felbamate
    * Gabpentin
    * Lamotrigine
    * Topiramate
    * Tiagabine
    * Levetiracetam
    * Oxcarbazepine
    * Zonisamide
    * Pregabalin
* Very new agents:
    * Lacosamide 
    * Rufinamide    
    * Vigabatrin
    * Clobazam
    * Ezogabine
    * Perampanel
    * Eslicarbazepine

#### Enzyme inducing AEDs
Drug | Enzyme
--- | ---
Phenytoin | &uarr;CYP2C9, CYP2C19
Carbamazepine | CYP3A4, CYP2C8, CYP1A2
Lamotrigine | UGT1A4
Phenobarbital | CYP3A4

### Mechanisms of AEDs
Drug | Mechanism
--- | ---
Benzodiazepines, barbiturates, felbamate, topiramate, zonisamide | &uarr;Cl<sup>-</sup> inward current, hyperpolarizing neuron resulting in inhibition.
Vigabatrin | Inhibits GABA-transaminase (GABA-T), &uarr; GABA neurotransmitter resulting in increased Cl<sup>-</sup> current, hyperpolarizing neuron resulting in inhibition.
Tiagabin | Binds to GABA uptake carrier (GATI) increasing synaptic GABA resulting in inhibition.
Phenytoin, carbamazepine, oxcarbazepine, valproic acid, felbamate, rufinamide, lamotrigine, lacosamide, topiramate, zonisamide | Decrease inward post-synaptic Na<sup>+</sup> currents, reducing excitatory transmission.
Gabapentin, pregabalin | Decrease inward pre-synaptic Ca<sup>2+</sup> currents, reducing excitatory transmission.
Ezogabine | K channel agonist, increases M-currents (inhibiting epileptic form activity)
Felbamate, topiramate | Inhibition of post-synaptic NDMA receptor
Perampanel, topiramate | Inhibition of post-synaptic AMPA receptor

### Adverse Reactions
Drug | AE
--- | ---
Lamotrigine | Rash (SJS/TEN), requires slow titration.
Carbamazepine | rash (HLA-B*1502), leukopenia
Felbamate | Fulminant hepatitis and aplastic anemia (BBW), aplastic anemia
Valproic acid | Hepatotoxicity, weight gain, thrombocytopenia
Vigabatrin | Permanent vision loss
Gabapentin | Weight gain
Pregabalin | Weight gain
Topiramate | Weight loss
Zonisamide | Weight loss
All AEDs | Suicidal ideation, osteoporosis/osteopenia

### Monitoring Parameters
Parameter | Example
--- | ---
Labs | CBC, LFTs, chemistry, Vitamin D
Therapeutic Drug Monitoring (TDM) | Drug levels
Physical/cognitive functions | -
Drug interactions | -
Mental status | Depression, suicidal ideation

### Therapeutic Evidence
Indication | Evidence Level A | Evidence Level B | Evidence Level C
--- | --- | --- | ---
Adult partial onset | CBZ, LEV, PT, ZNS | VPA | GBP, LTG, OXC, PB, TPM, VGB
Child partial onset | OXC | None | CBZ, PB, PHT, TPM, VPA, VGB
Elderly adult partial onset | GPB, LTG | None | CBZ
Adult generalize tonic-clonic | None | None | CBZ, LTZ, OXC, PB, PHT, TPM, VPA
Child generalizd tonic-clonic | None | None | CBZ, PB, PHT, TPM, VPA
Child absence | ESM, VPA | None | LTG
BECTS | None | None | CBZ, VPA
JME | None | None | None

### Drug indications
Generalized seizures | Partial seizures
--- | ---
Lamotrigine | Carbamazepine
Levetiracetam | Gabapentin/Pregabalin
Ethosuxamide (ab) | Lamotrigine
Topiramate | Levetiracetam
Valproic acid | Oxcarbazepine
- | Phenobarbital
- | Phenytoin
- | Topiramate
- | Valproic acid
- | Newere AEDs

* Lennox-Gastaut Syndrome:
    * BZD, CBZ, PB, PRM, PHT, VPA, VGB, FBM, TPM, LTG, RUF
* Juvenile myoclonic epilepsy (JME)
    * VPA, LTG, LEV, TPM, ZNS
    * CBZ may worsten JME

Monotherpy is preferred (no evidence suggests polytherapy is superior to mono). However,
titrate monotherapy to max dose and add second agent.

### Pregnancy
Many AEDs are teratogenic resulting in structural abnormalities of the fetus (i.e. neural
tube defects or urethra malformation in males, cardiac defects, oral clefts. Women with epilepsy should be counseled on contraception use (i.e. OC may not be effective if administered
with enzyme inducing AEDs, CYP3A4 inducers). Backup methods should be used in women taking
CYP3A4 inducing AEDs (Carbamazepine, Phenobarbital). Women should also receive **folic acid** supplementation before and during pregnancy. **Avoid valproic acid** unless absolutely necessary. Studies have suggested that valprate results in lower IQ of newborn when exposed prenatally.

### Menstruation
**Catamenial epilepsy** is a cyclical increase in seizure activity around the time of menses
or during other phases of the menstrual cycle. It is thought that etrogen is a pro-consvulsant while progesterone is an anti-convulsant.

##### Treatment
* Progesterone PO or vaginal suppository can be used to lessen seziure frequency.
Progesterone seems to potentiate the effects of GABA on post-synaptic neurons.
* Topiramate, zonisamide, acetazoramide and clobazam can also be used.

### Nonpharmacologic Therapy
* Surgery to correct structural abnormalities.
* Implant devices:
    * Vagas nerve stimulation (VNS)
    * Deep brain stimulation (DBS)
    * Responsive neurostimulation (RNS)
* Diet:
    * Ketogenic (high fat) diet may be useful in pediatric population.
    * Atkins diet (high protein)


#Lecture III - General Management of Poisoning
_by_ [_Michael L. Bernauer_](http://mlbernauer.bitbucket.org/)

#### Learning Objectives:
1. List the four strategies used in managing poisoned patients. Give examples of techniques associated with each strategy.
2. Know the dose, mechanism of action, indications, and contraindications for lavage, activated charcoal, cathartics, and whole bowel irrigations.
4. Correctly match antidotes to the poisons they are used to treat.
5. Utilize historical, laboratory and physical findings, in conjunction with given reference material, to correctly assess the toxidrome and severity of a potential poisoning.

## Poisoning Assessment: Toxidrome Approach
Toxidrome is a constellation of signs and symptoms which tend to occur consistently with a particular toxin.

Toxidrome | Toxins | Mechanism | Signs/Symptoms | Notes
--- | --- | --- | --- | ---
Sympathomimetic | Stimulants, sympathomimetics, sedative/hypnotic withdrawal | Metabolic overdrive, "fight-or-flight"| mydriasis, gut sounds, diaphoresis, seizure, hypertension, tachycardia, tachypnea | -
Antimuscarinic | Anticholinergics | Metabolic overdrive without DUMBBELS | Decreased; sweat, urine, gut motility. Presence of delirium, sedation, agitation, mumbling | -
Sedative/Hypnotic Toxidrome | Sedative/hypnotics | Metabolic slowdown | Decreased brain activity, sedation, coma, temp. dysregulation, variable pupil | -
Anticonvulsant | Phenytoin, phenobarbital, ethanol | CNS depression | Ataxia, nystagmus | -
Opiod | Heroin, oxycodone, methadone, fentanyl | - | Respiratory depression, miosis, HR, BP, temp variable, Triad: miosis, resp/CNS depresison | -

## Acid-Base Chemistry
Acid-base balance is useful in diagnosing certain toxidromes.
#### Anion Gap
Equation | Normal Range | Notes
--- | --- |
<i>AG = (Na<sup>+</sup> + K<sup>+</sup>) - (Cl<sup>-</sup> + HCO<sub>3</sub><sup>-</sup>)</i> | 10-12mEq/L | > 12mEq/L suggests unmeasured anions are present.

#### Assessment
Condition | pH | pO2 (mmHg) | pCO2 (mmHg) | HCO3 (mEq/L)
--- | --- | --- | --- | ---
Normal | 7.35 - 7.45 | 90 - 100 | 35-45 | 18-24
Metabolic Acidosis | &darr; | Norm | &darr; | &darr;
Respiratory Acidosis | &darr; | &darr; | &uarr; | Norm
Metabolic Alkalosis | &uarr; | Norm | &uarr; | &uarr;
Respiratory Alkalosis | &uarr; | &uarr; | &darr; | Norm

#### Anion Gap Acidosis
Drugs that are contributory to Anion Gap Acidosis:

***A-MUD-PILES***: ASA, AKA, MeOH, Metformin, Uremia, DKA, Phenformin, PG, Paraldehyde, Iron, INH, Lactic Acidosis, Ethylene Glyco, EtOH, Solvents, Starvation, Salicylates


## Radiology
Toxins that can be diagnosed using X-ray: KUB (kidney, ureter, bladder), CXR, US,
***CHIPES*** Chloral hydrate, Heavy metals, Iron/Iodine, Phenothiazines, Enterics, Solvents

## Four Strategies of Poison Treatment
#### Supportive care
Strategy | Assessment | Treatment | Notes
--- | --- | ---
Airway | Check cough/gag reflex if unconsciou | Positioning, suction, intubation | -
Breathing | Rate, pulse, ABGs (pH, O2, pCO2) | Mouth-to-mouth, bag-valve-mask/endotrachial tube, ventilator | -
Circulation | BP, HR, rhythm | Positioning, IV fluids (10-20mL/kg), norepinephrine, inotropes, antidysrhythmics | -
Altered Mental Status | temp, ABGs, Gluc, CT | ***DONT*** (**Dextrose**: A:D<sub>50</sub>W IV x 50mL, Peds:D<sub>25</sub>W IV x 2mL/kg; **O<sub>2</sub>**:8-10L/min, **Naloxone** 0.04-2mg IV, **Thiamine** 100mg IV) | -


#### Monitoring
Parameter | Abnormality | Cause
--- | --- | ---
12 lead ECG | - | -
Pulse oximetry | - | -
BP | -  | -
Temp | Hyperthermia | Sympathomimetics, salicylates and other uncouplers
Temp | Hypothermia | barbiturates, sedative-hypnotics

#### Pharmacologic Stabilization
Sutbstrate | Indication | Dose | Rate | Duration | CI
--- | --- | --- | --- | --- | ---
Naloxone, Nalmefene, Naltrexone | Opioid reversal | 0.04-2mg IV or nasal | gtt/hr is 2/3 respiratory stimulation dose | 30-60 min | -
Dextrose | hypoglycemia, CNS hypoglyrrachia | 0.25-1gm/kg IV push | D<sub>50</sub>W, D<sub>25</sub>W, D<sub>10</sub>W; Adult,Peds,Neonate | Also for ASA, &beta;-blocker, OHAs, toxic alcohol ODs | -
BZD antagonist (Flumazenil) | Respiratory depression | 0.1-0.2mg IV increments, max 3mg | - | 30-60min | BZD dependency, BZD for life threatening illness, TCA, INH, MAOI, cocain

#### Preventing Absorption
Route | Treatment
--- | ---
Inhalation | Fresh air/O<sub>2</sub>
Dermal | Irrigation/soap, remove cloathing
Ocular | Irrigation
Ingestion | Gastric lavage, Activated charcoal/cathartic, whole bowel irrigation

#### Absorption Treatment Modalities
Method | Dose |  Indications | CIs | Notes
--- | --- | --- | --- | ---
Ipecac | - | None | Many | Removes 30% of stomach contents
Emesis | - | - | CNS depression, corrosive ingestion, seizures, hemorrhagic diathesis, sharp object ingestion | -
GI decon. | - | liquid < 1hr, solid < 2hr | seizure, CNS depression, Alkaline, no gag reflex | -
Lavage| Adult:300cc H20, Peds:10cc/kg NS | Minimal, recent, charcoal not indicated | CNS depression, large preparations, underlying patho of esophagus/stomach | 30% reduction in _F_
Charcoal | 1gm/kg, Adult:30-60g, Peds:15-30g | - | - | 40% reduction in _F_
Saline (MgCitrate, MgSulfate, NaSulfate) | 4cc/kg (300cc), 250mg/kg (30g), 250mg/kg(30g) | - | - | May cause hypovolemia, electrolyte imbalance, DNE 1dose Q24h
Sorbitol | 0.5-0.9gm/kg (50gm)| - | -| May cause hypovolemia, electrolyte imbalance, DNE 1dose Q24h
PEG (GoLYTELY) | 500mL/hr (9mo-6yo), 1000mL/hr (6-12yo), >12yo 1.5-2L/hr | Modified release products, Fe, Li, K, Body packer | Perf., ileus, GI hemorrhage, obstruction, hemodynamic instability | 67% reduction in _F_, titrate to clear rectal effluent

**CN, Br, K, EtOH, MeOH, Fe, Li, Aklaline, Caustic, Mineral Acid, Hydrocarbons** do not adsorb well to activated charcoal.

## Other Elimination Methods
Method | Toxin characteristics | Dose | CIs
--- | --- | --- | ---
Hemodialysis | Low Vd (<1L/kg), single compartment kinetics, MW < 500Da, H2O soluble, low protein binding
MDAC | No effect on clinical outcome, may reduce elimination rates, | 0.5-1gm/kg Q2-6H, Ileus, obstruction, unprotected airway
Ion trapping (NaBicarb)| Phenobarbitaol, salicylates | 1-2mEq/kg Q3-4hrs urine pH > 7.5

## Converting Hydromorphone to Codeine Equivalents
Hydromorphone | Codeine | Lethal Codeine Dose
--- | --- | ---
7.5mg | 200mg | 7-14mg/kg


## Opioid Reversal
Agent | Onset | Duration
--- | --- | ---
Naloxone | 1min | 60min
Nalmefene | 1min | 60-240min

Agent | Bolus | CI | Monitoring
--- | --- | --- | ---
Naloxone | 0.4-10mg IV PRN Q1H | 10x initial dose in 1000mL D5W @ 100mL/hr | At least 2hrs after last dose of Naloxone

Choose reversal agent that has shortest duration of action, the allows providers to evaluate whether or not patient could relaps while still in the hospital.

**Naloxone** is preferred agent

## Summary of Treatment
#### Example: Hydromorphone OD

Strategy | Treatment
--- | ---
Supportive Care | O<sub>2</sub>, intubation, mech ventilation
Prevent Absorption | -
Enhancing Elimination | Not if toxin V<sub>d</sub> >4L/kg
Provide Antidote | Naloxone 0.4mg-10mg IV (if not on MechVent)
Monitor | Monitor patient at least 2hr after last Naloxone dose

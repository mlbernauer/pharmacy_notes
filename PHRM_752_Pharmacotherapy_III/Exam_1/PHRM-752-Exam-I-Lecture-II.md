# Lecture II - Anaphylaxis and Emergency Medicine Potpourri
_by_ [_Michael L. Bernauer_](http://mlbernauer.bitbucket.org/)

## Immunologic Classes of Allergic Drug Reactions
Class | Mechanism | Examples
--- | --- | ---
Type I | Anaphylaxis | Drug hapten reacts with IgE antibody on surface of mast cell/basophil resulting in release of mediators.
Type II | Cytotoxic-IgM, IgG involvement | Hemolytic anemia, thrombocytopenia, granulocytopenia
Type III | immune complex mediated-IgG | Serum sickness
Type IV | Cell mediated | Antigen binding to sensitized T-lymphocytes (contact dermatitis)

#### Causes of Anaphylaxis
Category | Examples
--- | ---
Food | Peanuts, tree nuts, shellfish, fish, milk, soy, wheat, eggs
Medications | Penicillin, sulfa antibiotics, radiocontrast media, antihypertensive
Insect sting | Bees, wasps, yellowjackets, hornets, fire ants
Seminal fluid | Coital anaphylaxis caused by IgE sensitization to semen/prostate fluid.
Latex | Rubber latex (dermatitis vs systemic allergic rxn)
Exercise | Unknown mechanism, meds or food may exacerbate
Allergen immunotherapy | increase risk in asthma and those on &beta; blocker
Idiopathic | Unknown origin

#### Signs and Symptoms of Anaphylaxis
System | Sings/Symptoms | Prevalence
--- | --- | ---
Cutaneous | Urticaria, angioedema, flushing, pruritis | 90%
Respiratory | Dyspnea, wheezing, upper airway edema, rhinits | 40-60%
Cardiac | Hypotension, dizziness, syncope, chest pain, arrhythmias | 30-35%
Abdominal | NV, diarrhea, cramping | 25-30%
Miscellaneous | Headache, seizure, diaphoresis | 5-8%, 1-2%

#### Treatment of Anaphylaxis
Therapy | Dose | Freq | Mechanism | SE | Onset | Duration
--- | --- | --- | --- | --- | --- | ---
Epinephrine | 1:1000 dilution (1mg/mL) Adult:0.3 - 0.5mg, Peds 0.01mg (max 0.3mg) IM | 5-15 min | Tachycardia, hypertension, palpitations, NV, tremor | sympathomimetic (&beta; &alpha;-agonist) | rapid | short
EpiPen | 0.3mg (Adult), 0.15mg (Jr) | - | - | - | - | -
Inhaled Epi | 0.025% racemic | - | - | - | - |-
Oxygen +/- &beta; agonist | Nasal cannula, face maks, non-rebreater | - | - | - | - | -
NS | Bolus 1-2L | - | NS stays in vasculature longer than dextrose | Dont use if normotensive | - | -
Vasopressor | - | - | Increase CO | Dont use if normotensive/hypertensive | - | -
Diphenhydramine | Adult:25-50mg, Peds:1-2mg/kg IV/IM NTE 25mg/min | Adult:Q2, Peds:Q4 NTE 400mg/day,300mg/day | H1 antagonist | Tissue necrosis | PO:15-30min, IV:rapid | 4-6hrs
Ranitidine, famotidine, cimetidine | A:50mg, P:1-2mg/kg (50mg max) IV | - | H2 antagonist | Use only with epi or H1-antag | Slower than H1 blocker | Varies
Albuterol | 2.5-5mg in 3mL NS Neb. | PRN | &beta;-agonist | - | 5-15min | 4-6hrs
Methylprednisolone | 0.5 - 1mg/kg IV | - | Prevents biphasic,protracted RNXs | - | varies, 1-2hrs | -
Hydrocortisone | 0.5-1mg/kg | - | Prevents bisphasic, protracted RXNs
Glucagon | A:1-5mg IV over 5min, P:20-30mcg/kg then CI at 5-15mcg/min | CI | Bypasses &beta; receptor stimulating adenylate cyclase directly reversing brocnchospasm/hypotension | - | - | -

#### Patients on &beta;-blockers
&beta;-blockers block the effects of epinephrine which can worsen anaphylaxis. In these situations Glucagon can be used. Glucagon works by bypassing &beta; receptor and activating adenylate cyclase directly to reverse bronchospasms and hypotension.

#### Cardiopulmonary Arrest during Anaphylaxis
1. First dose: 1-3mg of 1:10000 (1mg/10mL)
2. Second dose: 3-5mg IV over 3 min
3. Third dose: 4-10mcg/min infusion

#### ACE-I Induced Angioedema
Frequently does not respond well to standard anaphylaxis therapy. Thought to be due to excessive bradykinin release.

Therapy | Mechanism
--- | ---
Fresh Frozen Plasma (FFP) | Contains kininase II (identical to ACE) that can break down bradykinin.
C1 esterase inhibitor concentrate (Berinert, Cinryze) | Used to treat/prevent hereditary angioedema

## Rabies
Acute, progressive, fatal encephalomyelitis caused by neurotropic virus. Most often caused by bite from infected animal (raccoon, skunk, cat, fox, bats) sometimes scratches and inhalation. Virus travels from synapse to brain. Post exposure prevention is based on preventing virus from entering peripheral nerves. Symptoms include signess, fever, agitation, spasms, difficulty swallowing, salivation.

#### Available Treatment
Therapy | Formulation | Dose | Route
--- | --- | --- | ---
Human rabies immunoglobulin | Injection 150mg/mL | 20 IU/kg | injected into bite site (arm, leg, butt if not able to admin all to bite site)
Rabies vaccine | Human diploid cell vaccine, chick embryo cell culture vaccine | - | A:deltoid, P:lateral thigh

#### Pre-exposure Therapy
Therapy | Schedule | Notes
--- | --- | ---
Immunization | 0, 7, 21 or 28 days | If exposed no HRIB needed, two shots days 0, 3

#### Post-exposure Therapy
Immune status | Therapy | Schedule
--- | --- | ---
Immuneocompetent | Vaccine/Immunoglobulin | 0, 3, 7, 14 days
Immunocompromised | Vaccine/Immunoglobulin | 0, 3, 7, 14, 28 days
Prior immunization | Vaccine | 0, 3 days

## Headaches
Migraine presents as unilateral or bilateral with or without aura (classic vs common). Typicall associated with N/V, photophobia, phonophobia, dizziness. Exact cause unknown

#### Treatment
Therapy | Mechanisms | Dose
--- | --- | ---
Ketorolac | NSAID | 30mg IV or 60mg IM
Sumatriptan (Imitrex) | 5-HT1 antagonist | 5mg SC or 5,10,20mg intranasally
Zomatriptan (Relpax) | 5-HT1 antagonist | 2.5mg or 5mg
Rizatriptan (Maxalt) | 5-HT1 antagonist | 5 or 10mg
Prochlorperazine | Phenothiazine, antiemetic| 10mg IV in 1L NS, 25mg diphenhydramine pretreatment
Droperidol | D2-antagonist, Antiemetic |

Post lumbar puncture headaches occur in 10-30% of LP patients resulting from leakage of CSF. Treat with **analgesics**, **caffeine sodium benzoate 500mg in 1L** or **epidural blood patch**.

## Post-traumatic seizure prophylaxis
Prophylaxis is defined as protection against acute seizures within 7 days of insult. Long term prophylaxis offers no long term benefit.

#### Treatment Options
Therapy | Dose  | Notes
--- | --- | ---
Phenytoin/Fosphenytoin | 20mg/kg or 20 PE/kg; 100mg TID maintenance | Newer agents not studied
Valproate | - | Seizure rates similar to phenytoin, higher mortality w/ valproate, not rec. for post-traumatic seizure prophylaxis
Levetiracetam | Not established | Titrate to response, alternative thearpy, phenytoin more cost effective.

#### Recommended Therapy
Early is within first week of insult.

Late is > 1 week up to 10 years post insult.

Indication | Therapy | Dose | Note
--- | --- | --- | ---
Early post-traumatic seizure | Fosphenytoin | 20mg/kg PE load | Long term treatment needed
Late post-traumatic seizure | Fosphenytoin | 20mg/kg PE load | Long term treatment needed

## Ear Conditions
Indication | Therapy | Dose | Mechanism | Notes
--- | --- | --- | --- | ---
Ear wax | Docusate sodium | 1mL of 10mg/mL soln 15min prior to irrigation | Cerumenolytic agent | Caution tympanic membrane status
Bugs in ear | Mineral oil | Fill canal | Suffocation | Flush with water or NS

## Nose Administration
Therapy | Dose | Indication | Note
--- | --- | --- | ---
Midazolam | Peds: 0.2-0.4mg/kg | No IV access, seizure, sedation | -
Fentanyl | Adult:50-200mcg, Peds:1-2.5mcg/kg | No IV access | -
Lidocaine | 4% soln | NG tube placement | -
Naloxone | - | opiate reversal | -


## Miscellaneous Conditions

Therapy | Dose | Freq | Mechanism | Indication | Note
--- | --- | --- | --- | --- | ---
Glucagon | 1-2mg IV | 10-20min | Relax LES | Foreign body/epigastric discomfort | little data
Endoscopy | - | - | Remove foreign body | Foregn body/epigastric discomfort | -
GI cocktail (maalox, lidocaine 2%, donnatal) | - | - | Dyspepsia | inappropriately masks cardiac pain | -
Haloperidol | 2-5mg PO | Q4-8hr | - | Intractable hiccups | -
Chlorpromazine | 25-50mg PO | TID-QID | - |  Intractable hiccups | -
Metoclopramide | 10-20mg PO  TID-QID x 7d | - | - |Intractable hiccups | -
Baclofen | 10mg PO | BID-QID | - | Intractable hiccups | -
Carvedolol | 6.25mg PO | QID | - |Intractable hiccups | -
Olanzapine | 2.5mg PO | QD | - | Intractable hiccups | used post TBI
Phenytoin, valproic acid, carbamazepine | - | - | - | Intractable hiccups | -
Gabapentin | - | - | - | intractable hiccups | use when CNS lesions present

## Ectopic Pregnancy
Symptoms similar to normal pregnancy until 6-8wks, patient present with abdominal pain, vaginal bleeding, diagnosis made by ultrasound.

#### Treatment
Thearpy | Dose | Freq | Mechanism | Indication | Note
--- | --- | --- | --- | --- | ---
Methotrexate | 50mg/m<sup>2</sup> IM | PRN (20% pts) | Inhibits cell growth | Chemo handling, monitor hCG (should see 15% decrease in 4-7d)

## Emergency Delivery
Only imminent delivery and post-delivery patients are seen in ED, all others are triaged to labor and delivery (L&D). Goal is to encourage contractions, reverse uterine atony, and compress uterine vessels to stop bleeding.

#### Pharmacologic Treatment
Therapy | Dose | Mechanism | Note
--- | --- | --- | ---
Oxytocin (Pitocin) | 10-40U in 1L of fluid | Increase intracellular Ca<sup>2+</sup> causing contractions, stop bleeding. | -
Methylergonovine (Methergine) | 0.2mg PO TID-QID (max 7d), 0.2mg IM Q2-4H (max 5 doses) | Increase contraction force/freq | -
Carboprost tromethamine (Hemabate) | 250mcg IM (2mg max) | Prostaglandin, causing uterin contraction | -
Misoprostol (Cytotec) | 600-800mcg PO/PR | - | -

## Miscarriage
**Complete**-all products of conception emptied from uterus. **Threatened** - early pregnancy bleeding, cramping suggestive of possible miscarriage. **Incomplete** miscarriage - abdominal pain, bleeding, open cervix, not all contents emptied.

#### Pharmacological Treatment
Therapy | Dose | Mechanism | Note
--- | --- | --- | ---
Oxytocin | 10-20U/L | Uterine contractions | lower dose than post-delivery
Misoprostol | 800 mcg PO/PV/PR | lower doses not as effective | -


## Rhesus (Rh) Factor
All patients should have blood typed. Rh(D) + positive patients need no further treatment. Rh(D) - patients receiving Rh(D) + blood should receive **Rho(D)** Immunoglobulin (RhoGAM) which suppresses immune response to Rho(D) positive RBCs

#### Pharmacological Treatment
Indication | Therapy | Dose | Notes
--- | --- | --- | ---
Rh(D) - Patients | Rho(D) Immunoglobulin (RhoGAM) | 300mcg IM with 72 hrs | -

## Priapism
Reported in 35% of sickle cell anemia patients, 0.4-4% of patients using alprostadil (Caverject). Typically caused by antihypertensives, psychotropic medications, illicit drugs. Etiology may be hematologic, or metabolic. **Low-flow priapism** is type of veno-occlusive priapism in which decreased outflow results in prolonged, painful erection. **High-flow priapism** is usually associated with trauma of the pelvic region and results from uncontrolled arterial inflow.

#### Pharmacologic Treatment
Indication | Therapy | Dose | Mechanism | Notes
--- | --- | --- | --- | ---
All patients | dorsal nerve block, pain control, anxiolytics, ice, elevation | - | - | -
SCA patients | hydration + oxygen, RBC transfusion | - | - | Usually do not need pharmacologic agent
Low-flow | Terbutaline | 5-10mg PO, 0.25 - 0.5 SC | Unclear | Questionable efficacy
Low-flow | Pseudoephedrine | 60-120mg | vasoconstriction via &alpha;-activation | Questionable efficacy
Low-flow | Aspiration of corpora cavernosa | - | - | -
Low-flow | Phenylephrine | 10-20mcg/mL IM penis | &alpha;-agonist vasoconstriction | -
Low-flow | Epinephrine | - | - | -
Low-flow | Methylene blue | - | - | -

# P3 Spring Lecture Notes 2015
_by_ [_Michael L. Bernauer_](http://mlbernauer.bitbucket.org/)

[GitHub](https://github.com/mlbernauer/pharmacy#p3-spring-lecture-notes-2015)

### PHRM 752 - Pharmacotherapeutics III
####Exam I
1. [Lecture I - Anesthetics, Epistaxis, and Prophylaxis](https://bitbucket.org/mlbernauer/pharmacy_notes/src/master/PHRM-752-Pharmacotherapy-III/PHRM-752-Exam-I-Lecture-I.md)
2. [Lecture II - Anaphylaxis and Emergency Medicine Potpourri](https://bitbucket.org/mlbernauer/pharmacy_notes/src/master/PHRM-752-Pharmacotherapy-III/PHRM-752-Exam-I-Lecture-II.md)
3. [Lecture III - General Management of Poisoning ](https://bitbucket.org/mlbernauer/pharmacy_notes/src/master/PHRM-752-Pharmacotherapy-III/PHRM-752-Exam-I-Lecture-III.md)
4. [Lecture IV - Poison Prevention and Antidotes](https://bitbucket.org/mlbernauer/pharmacy_notes/src/master/PHRM-752-Pharmacotherapy-III/PHRM-752-Exam-I-Lecture-IV.md)
5. [Lecture V - Toxicokinetics](https://bitbucket.org/mlbernauer/pharmacy_notes/src/master/PHRM-752-Pharmacotherapy-III/PHRM-752-Exam-I-Lecture-IV.md)
6. [Lecture VI - Weapons of Mass Destruction](https://bitbucket.org/mlbernauer/pharmacy_notes/src/master/PHRM-752-Pharmacotherapy-III/PHRM-752-Exam-I-Lecture-IV.md)

### PHRM 784 - Advanced ID Pharmacotherapy
#### Exam I
1. [Lecture I]()
2. [Lecture II]()
3. [Lecture III - Antimicrobial Stewardship Programs](https://github.com/mlbernauer/pharmacy/blob/master/PHRM-784-Advanced-ID-Pharmacotherapy/PHRM-784-Exam-I-Lecture-III.md)

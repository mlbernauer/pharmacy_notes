# PHRM 764 - Basic Principles of Pharmacogenomics
_by_ [_Michael L. Bernauer_](http://mlbernauer.bitbucket.org/)

## Learning objectives
1. Be able to define pharmacogenomics.
   * Science that examines inherited variations in genes that dictate drug
   response. Explores the way these variations can be used to predict whether
   a patient will have a good or bad response to a drug or no response at all.
   Enabled by high-throughput genome analysis techniques.
2. Recognize the future role of pharmacogenomics in pharmacy practice.
   * Efficiency: Pharmacists may provide greater value for drug therapy by
   providing more efficient use of pharmaceuticals to reduce their costs.
   * Accuracy and safety: May contribute to dispensing accuracy, screening
   prescriptions, and providing patient education.
   * Quality: Increase collaborative drug therapy among physicians.
   * Access: Increase compounding.
3. Consider new responsibilities that pharmacists will incur with the advent of
   pharmacogenomics.
   * Collect samples for delivery to DNA analysis clinic.
   * Provide information on results of DNA analysis used for optimizing drug
   therapy.
   * Describe why a genetic test resulted in a prescription for a particular
   medication.
   * Explain why, based on a genetic test, particular drug or dosage form is used.
   * Advise about potential drug side-effects that may be suggested from results
   of a genetic test.
   * Increase dispensing of drugs for disease prevention.
4. Know basic concepts of DNA.
   * Purines: Adenine, Guanine
   * Pyrimidines: Cytosine, Thymine
5. Understand the importance of the biological dogma and its relevance to
   pharmacogenomics.
   * DNA &rarr; (transcription) &rarr; mRNA &rarr; (translation) &rarr; Protein
6. Be able to associate the parts of a gene with the relevant structural components
   of a protein.
   * Promoter region
   * Transcriptional Initiation Site
   * 5' Untranslated Region (5' UTR)
   * Coding Region (Exons)
   * Introns
   * 3' Untranslated Region (3' UTR)
   * Polyadenylation Signal
7. How are polymorphic gene variants manifested in proteins? What is a functional
   variant?
   * Variants manifest as amino acid substitutions, functional variants are proteins
   that deviate from what is considered 'normal' while still retaining their
   function, however their function may be slightly altered, i.e. less active/more
   active.
8. Understand how genetics may affect drug response.
   * Population can be broadly categorized as "risistant individuals" (minimal
     drug effect), "Majority of individuals" (average drug effect), "Sensitive
     individuals" (maximum drug effect).
   * Within the context of CYP450s
      * __Poor Metabolizers__ - awo variant alleles, no enzyme activity.
      * __Intermediate Metabolizers__ - one reduced activity allele/null allele
      * __Extensive Metabolizers__ - at least one normal allele
      * __Ultrarapid Metabolizers__ - Multiple functional alleles, excess
      enzymatic activity.
9. Know a key law protecting personal genetic information (i.e. GINA)
   * US legislation that is designed to prohibit improper use of genetic
   information by health insurance and employment. Prohibits agencies from denying
   coverage or increasing premiums based on genetic predisposition. Bars employers
   from using genetic information in hiring/firing/placement/promotion decisions.

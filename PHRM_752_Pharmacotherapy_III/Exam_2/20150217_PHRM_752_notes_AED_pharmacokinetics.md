# PHRM 752 - AED Pharmacokinetics

## Learning Objectives
1. Describe the basic ADME (absorpition, distribution, metabolism, and exretion/elimination
) of antiepileptic drugs.
2. Explain the mechanisms of drug-drug interactions using PK/PD theory between AEDs and
between AED and other concomitant medications.
3. Differentiate between zero-order kinetics and first-order kinetics.

## Notes

### Kinetics of Older AEDs
* Non-linear pharmacokinetics (Michaelis-Menten) - Clearance decreases as dose increases (PHT)
* Linear pharmacokinetics - The rate of clearance remains the same as dose increases (PB, VPA)

### Kinetics of Newer AEDs
* Linear pharmacokinetics - The rate of clearance remains constant as the dose increaseas (FBM, LEV, OXC, TGB, TMP, ZNS)

### Drug kinetics
Nonlinear (zero order) | Linear (first order)
--- | ---
Phenytoin (PHT) | Phenobarbital (PB)
- | Valproic acid (VPA)
- | Felbamate (FBM)
- | Levetiracetam (LEV)
- | Oxcarbazepine (OXC)
- | Tiagabin (TGB)
- | Topiramate (TPM)
- | ZNS

* Zero order elimination kinetics - the clearance rate does not depend on concentration of the drug, thus the clearance rate remains unchanged as more drug becomes eliminated.
* First order elimination kinetics - the clearance rate does depend on concentration of the drug, thus the clearance rate decreases as serum concentration decreases.

### Dose adjustments
Example 1:

A patient comes into the clinic with reports of poorly controlled seizures. The patient
has been on oxcarbazepine. Trough levels from 6 months ago are 11.34&mu;g/mL. Todays trough
levels are 11.0&mu;g/mL with a current oxcarbazepine dose of 450mg PO BID (900mg/day). How would you adjust this medication?

Oxcarbazepine demonstrates first order elemination kinetics. Plasma concentrations for this drug are directly proportional to dose given, i.e. a 20% increase in dose administered
will result in a 20% increase in trough concentration. We can express this relationship
in the following way

(D<sub>current</sub> / C<sub>current</sub>) = (D<sub>desired</sub> / C<sub>desired</sub>)

The desired concentration is 15&mu;g/mL therefore we must use D<sub>desired</sub> = (900mg)x(15&mu;g/mL) / (11&mu;g/mL) = 1222mg or 600mg BID

# PHRM 764 - Vaccine Trial
_by_ [_Michael L. Bernauer_](http://mlbernauer.bitbucket.org/)

## Assignment

***Please address the following questions regarding your assigned vaccine trial(s):***

### Trial #:
NCT00399529

### What clinical phase is the trial?
Phase 2

### Is the vaccine prophylactic or therapeutic?
Therapeutic. The main purpose of this study is to test the safety, clinical benefit,
and bioactivity of vaccine therapy in combination with cyclophosphamide and
trastuzamab in patients with HER-2/neu overexpressing stage IV breast cancer.

### What is the specific disease or pathogen targeted by the vaccine?
The study includes patients with histologically confirmed HER-2/neu overexpressing
adenocarcinoma of the breast.

### Is this an autologous vaccine? If yes, what makes it autologous?
No. The vaccine is an allogeneic mixture of CM-CSF secreting breast cancer cell lines.

### What is/are the antigen(s)? Why are these good antigens?
The vaccine works by enhancing the immune response to breast cancer cells overexpressing
HER-2/neu by secreting CM-CSF, not by administration of antigen.

### Does the vaccine include adjuvants? Describe them - how do they work?
Because the vaccine enhances immune response through the release of CM-CSF, it can be thought of as an adjuvant. Cyclophosphamide augments vaccine activity in tolerant neu mice and in patients
with metastatic breast cancer. HER2-specific monoclonal antibodies (mAb) enhance vaccine
activity in neu mice. The allogeneic cell lines used in the vaccine secrete human
granulocyte-macrophage. 
colony-stimulating factor (GM-CSF) which functions as a cytokine, activating the
immunse system to produce granulocytes (neutrophils, eosinophils, and basophils) and monocytes.


### Does the vaccine include a delivery vector? If so, please describe how it works.


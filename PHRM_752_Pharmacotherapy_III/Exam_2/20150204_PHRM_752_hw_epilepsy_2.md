# PHRM 752-Epillepsy HW II
_by_ [_Michael L. Bernauer_](http://mlbernauer.bitbucket.org/)

## Instructions
Read the following case to answer questions 1 and 2

ST is a 78yo African American female (height: 5'4", weight 170lbs) and has sudden onset of seizures (complex partial seizures) after 5-day hospitalization. Following is current ST's medication list and today's lab:

### Medications
* Imipenem/Cilastatin 500mg Q6H for osteomylitis (Day 5)
* Acyclovir IV: 10mg/kg/dose every 8 hours for HSV encephalitis (suspected) (Day 3)
* Levetiracetam 750 mg PO BID (Day 5)
* Phenytoin 100mg QAM and 75mg QPM (Day 5)

### Labs
* Total PHT level: 15.3 mg/L
* Free PHT level: 5.8 mg/L
* Albumin 2.0 (2.2 at admission)
* Cr: 2.0 (1.2 at admission)

## Questions
### Question #1
a) What is the normal therapeutic range of phenytoin concentration (total and free)?

Drug | Fraction | Age | Concentration
--- | --- | --- | ---
Phenytoin | Total | children/adults | 10-20mg/L
Phenytoin | Free | - | 1-2mg/L


b) Is ST's estimated PHT level high, normal, or low?

* ST's total phenytoin level is normal (15.3mg/L), however her free phenytoin level is high (5.8mg/L)

### Question #2
Why does such a relatively small dose of phenytoin give a higher PTH level?

* Phenytoin exhibits non-linear, zero-order elimination kinetics which suggests there is a non-linear response between dose given and serum concentration.

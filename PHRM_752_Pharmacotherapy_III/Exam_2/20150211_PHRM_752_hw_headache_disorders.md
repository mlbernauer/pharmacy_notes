# PHRM 752 - Headache Disorders
_by_ [_Michael L. Bernauer_](http://mlbernauer.bitbucket.org/)

02/11/2015
### Case:
K.L. A 29 year old woman, visits the clinic and complains about her 5-month history of left-sided pulsatile head pain recurring on a weekly basis. Her headaches usually are preceded by unformed flashes of light bilaterally and a sensation of lightheadedness. The pain is always unilateral and is commonly associated with nausea, vomiting, and photophobia. The headache is not relieved by 2 tablets of either aspirin 325mg or ibuprofen 200mg and generally lasts all day unless she is able to lie in a dark room and sleep. The headaches usually interfere with her ability to continue to work.

Six months later, K.L. returned to the clinic and said her health insurance no longer covers her triptan medications.

---
### Question 1
***What medication is an alternative for K.L.'s migrane headaches? Assume she is not pregnant, breast-feeding, and does not have a current and history of serious diseases, such as renal insufficiency and liver disease.***

There are several medications that may be used as alternatives to triptans. Alternative agents can be broken up into several categories; Nonpharmacologic, Pharmacologic, over the counter and prescription only. The table below describes these alternatives.

Agent | Category | Comment
--- | --- | ---
Rest/sleep, Ice/hot-pack | Nonpharmacologic | Rest in dark, quite environment
Acetaminophen, Aspirin, NSAIDS | OTC | Analgesics, do not exceed 4g QD
Topical (HEAD-ON) | OTC | -
Ergotamine tartrate, Dihydroergotamine | Prescription | Ergotamine derivatives
Butorphanol spray, Metoclopramide, Prochlorperamine | Prescription | -



### Question 2
***Describe the pharmacology of Carefgot (ergotamine tartrate + caffeine).***

#### Mechanism of Action:
Ergots derivatives are partial agonists at &alpha;-adrenoceptors and 5-HT receptors, producing constriction of peripheral and cranial blood vessels. Caffeine, a methylxanthin, exerts pharmacological effects by increasing calcium permeability in sarcoplasmic reticulum, this results in the inhibition of phosphodiesterase promoting the accumulation of cAMP, it is also a competative, non-selective inhibitors of &alpha;<sub>1</sub> and &alpha;<sub>2A</sub> receptors. Caffeine usefulness in treating headaches stems from its ability to cause constriction of the cerebral vasculature. [source](http://online.factsandcomparisons.com/MonoDisp.aspx?monoID=fandc-hcp12448&quick=239577%7c5&search=239577%7c5&isstemmed=True&NDCmapping=-1&fromTop=true#firstMatch)

#### When to use ergotamine tartrate:
Ergotamine tartrate is used to abort or prevent headache in patients without coronary heart disease, hypertension, hepatic or renal function impairment, sepsis, pregnancy, or if patient is on a potent CYP3A4 inhibitor (ritonavir, nelfinavir, indinavir, erythromycin, clarithromycin, troleandomycin). [source](http://online.factsandcomparisons.com/MonoDisp.aspx?monoID=fandc-hcp13285&quick=349470%7c5&search=349470%7c5&isstemmed=True&NDCmapping=-1&fromTop=true#firstMatch)
#### Adverse reactions:
**Cardiovascular** adverse reactions are associated with ergotamine tartrate's vasoconstrictive properties and include ischemia; cyanosis, absence of pulse, cold extremities, gangrene, ECG changes and muscle pains. These effects are most commonly associated with long-term use but can occur with short term use as well.

**CNS** effects include numbness, paresthesias, vertigo, and weakness.

**GI** effects include nausea and vomiting.

Some patients experience dermatological effects resulting from hypersensitivity. These reactions include itching and localized edema.

#### Contraindications:
Ergotamine tartrate is contraindicated in patients with peripharal vascular disease, coronary heart disease, hypertension, hepatic or renal insufficiency, sepsis, hypersensitivity, pregnancy and concomitant CYP3A4 inhibitor use.

### Question 3
***If K.L. has migraine attacks and vomiting associated with her migraine headaches, which made it difficult to take oral ergot medication, what would be an alternative administration route in an outpatient setting?***

If oral administration is not an option the patient may take intranasal butorphanol. Butorphanol is a mixed agonist-antagonist with low intrinsic activity at the &mu;-opioid type (morphine-like) receptors as well as a &kappa;-opioid receptor agonist.

# Pharmacokinetics of Antiepileptic Drugs
_by_ [_Michael L. Bernauer_](http://mlbernauer.bitbucket.org)

## Learning Objectives
1. Describe basic ADME (absorption, distribution, metabolism, and elimination) of antiepileptic drugs.
2. Explain the mechanism of drug-drug interaction using PK/PD theory between AEDs and between AED and other concomitant medications.
3. Differentiate zero-order kinetics and first-order kinetics.

## Notes
### Kinetics
Kinetics defines the relationship between dose and serum concentration as well as dose and clearance.

### Kinetics types
Type | Example | Description
--- | ---
Nonlinear (Michaelis-Menten) | PHT | Clearance decreases as dose increases
Linear | PB, VPA (unbound) | Clearance remains constant as dose increases
Nonlinear | CBZ, VPA (total) | Clearance increases with dose/Absorption decreases with dose

### Elimination
Zero-order | First-order
--- | ---
Phenytoin (PHT) | Phenobarbital (PB), Valproic acid (VPA), Felbamate (FBM), Levetiracetam (LEV), Oxcarbazepine (OXC), Tiagabin (TGB), Topiramate (TPM)


### Kinetics equations
Type | Elimination | Serum Conc. | Elimination T1/2
--- | --- | --- | ---
Zero-order |Const (-dA/dt = kz) | Linear (A' = A - kt) | t1/2 = A/(2kz)
First-order | Prop to drug conc. (-dA/dt = Ak) | Decrease w/time (A = Ae^(-Kt)) | t1/2 = 0.693/(k)

### First-order Elimination Example
For first order kinetics, the dose required to reach a certain
concentration can be calculated by using the following;

(Current Dose)/(Current Conc.) = (New Dose)/(Desired Conc.)

### Determinants of Distribution
Determinant | Example
--- | ---
Distri

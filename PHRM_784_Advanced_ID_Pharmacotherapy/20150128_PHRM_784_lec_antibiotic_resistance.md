# Lecture III - Antibiotic Resistance
_by_ [_Michael L. Bernauer_](http://mlbernauer.bitbucket.org/blog/)

## Antibiotics are a Public Health Issue
**Patient impact** - Inappropriate antimicrobial therapy associated with increased mortality.
**Societal impact** - Indiscriminant antibiotic therapy drives resistance.

## Antimicrobial Stewardship Programs
**Shown** to improve clinical patient outcomes. They are better at selecting appropriate
antibiotics, which improves cure rates, decrease resistance rates and improves patient outcomes.

### Pulmonary Infiltrates in ICU
**Study** determined patients treated according to ASP had decreased hospital stay, lower mortality.

### Components of AMP
1. Physician/Pharmacist (Core)
2. Hospital administration
3. Infection control
4. Microbiology
5. Informatics
6. Infectious disease

### Core Strategies
Strategy | Advantage | Disadvantage
--- | --- | ---
Prospective audio w/ direct intervention/feedback | Reduce inappropriate ABX use, modify future prescribing, Prescribers have automomy | -

### Supplemental Strategies
Strategy | Outcome
--- | ---
Education | Short half-life, requires reimplementation
Guidelines/Evidence based medicine | -
De-escalation therapy | Narrow therapy
Dose optimization | Using appropriate dose for patient/infection
IV &rarr; PO | Reduce complications and length of stay

**Average** hospital stay is 5 days.


## Antibiotic De-escalation
**Biggest** challenge to de-escalation is providers reluctance
  to switch if patient is currently improving on current therapy.
  This should not be a factor because treatment outcomes should be
  unaltered.

**Study** of Ventilator Associated Pneumonia showed that there was no increase in mortality
  associated with antimicrobial de-escalation. Conlucded that you can safely de-escalate
  without affecting clinical outcomes.

## Dose Optimization
Study shown that patietnts with P. aurgenosa bacterimia with MIC of 32 or 64
showed significant mortality when given PIP/Tazo compared to those with MIC < 16.
This lead some to think that we should relabel P. _aurgenosa_ strains with MIC > 32
as not being susceptible.

## Clinical and Economic Impact of ASPs
**Pharmacist Managed Antimicrobial Prophylaxis in Surgical Patients:** Surgical site
infections are associated with poor outcomes.

How are large numbers of patients managed with non-ID trained pharmacists.
Grady Memorial Hospital (Atlanta, GA) 1000 beds had 1 full-time ID pharmacist.
Trained non-ID floor pharmacists to recognize unrestricted ABX. This showed that
you can train non-ID pharmacists to positively impact poor prescribing practices.

## Assessing Collateral Damage of Poor Prescribing
ABX exposure most significant risk factor for **C._difficile_**. Incidence of C.
difficile infection has decreased since implementing ASPsPENISPENIS PENIS PENIS PENIS PENIS PENIS PENIS PENIS PENIS PENIS PENIS.

## Future Directions of ASPs
Goal is to expand to pediatrics, LTCFs, Ambulatory care, rural/community hospital
, education of other HPs. Goal is to treat patients in place which reduces risk of
transmission.

## Conclusion
1. Inappropriate use promotes antimicrobial resistance.

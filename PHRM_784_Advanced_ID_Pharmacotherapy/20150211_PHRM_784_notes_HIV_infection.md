# PHRM 784 - HIV Infection Notes
_by_ [_Michael L. Bernauer_](http://mlbernauer.bitbucket.org/)

## Learning Objectives
1. Evaluate patient characteristics to determine the optimal regiment for treatment-naive patients with HIV infection.
2. Apply available data in determining the use of new anti-retrovirals in both treatment-experienced and treatment-naive patients with HIV infection.
3. Assess the appropriateness of HIV pre-exposure prophylaxis on an individual-patient basis.
4. Assess the appropriateness of and design a regimen for HIV post-exposure prophylaxis.
5. Design and appropriate treatment regimen for a patient presenting with transmitted HIV resistance or an antiretroviral-therapy-treatment-experienced patient.
6. Develop a monitoring plan for drug interactions and adverse event associated with HIV treatment.

### Abbreviations
1. ART:  Antiretroviral therapy
2. ARV: Antiretroviral
3. DHHS: Department of Health and Human Services
4. INSTI: Integrase Strand Transfer Inhibitor
5. NNRTI: Nonnucleoside Reverse Transcriptase Inhibitor
6. P-gp: P-glycoprotein
7. STI: Sexually Transmitted Infection

### Epidemiology
There was a 33% decline in new HIV infections from 2001-2012. Deaths attributable to AIDS decreased from 2.3 million to 1.6 million from 2005-2012. As a result, we now have more people living with HIV infection (35.3 million) than in previous years.

MSM, African-Americans, Hispanics and women of color are at highest risk for HIV infection.

### Transmission and Testing
CDC recommends high-intesnity behavioral counseling for all sexually active adolescents and adults at risk of STI/HIV infection. Counseling should focus on behavioral modifications that can reduce risk of infection; i.e. injection/non-injection drug use, EtOH use.

All patients aged 13-64 should be tested for HIV regardless of high-risk behavior.

### Advancements in HIV Testing
First at-home HIV test (OraQuick, $40) approved July 2012 for individuals 17years and older (99.98% specificity). False negatives may be reported if infection occurred less than 3 months ago.

A fourth-generation immunoassay was approved in 2010 which detects p24 antigen which can be detected much earlier than antibodies.

### Treatment as Prevention
HPTN052 study demonstrated that early initiation of ART reduces the rate of sexual transmission of HIV in serodiscordant couples.

### Early Infection
**Early infection** describes an acute infection (immediately after HIV infection before seroconversion) where **recent HIV infection** is the period within 6 months of initial infection.

Early infection is characterized by high viral replication rates (1 million copies/mL) with most replication occurring in reservoirs such as genital tract making transmission likely.


### Presentation
HIV infection can occur asymptomatically however 75% of patients have mononucleosis-like syndromes including rash, fever, pharyngitis and headache within 2-6 weeks.

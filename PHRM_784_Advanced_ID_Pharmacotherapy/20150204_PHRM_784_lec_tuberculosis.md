# Tuberculosis
_by_ [_Michael L. Bernauer_](http://mlbernauer.bitbucket.org/)

## Tuberculosis prevalence
In the US tb treatment is free of charge, this has resulted in dramatic decrease in TB cases over time.

## Race/Ethnicity
African Americans are at increased risk for TB than white/hispanic. The largest racial group affected by TB among foreign born are Asian (46%) with Hispainc/Latino coming in second (34%).

## Pathophysiology
Airborn disease (not via food, shared utensils etc) small infectious particle (5 &mu;M). 85% of the disease resides in the lungs, this also the way it is reintroduced to the environment (exhaled particles). MDR strains exist, can be death sentence.

Type | cases | deaths
--- | --- | ---
All TB | 9.3M | 1.7M
MDR-TB | 500K | 150K
XDR-TB | 50K | 30K

## Diagnosis
Usually active disease presents with infection; cough, wight loss

## Treatment
Standardized throughout the world. RIP is mainstay of therapy,
ethambutol is only there to prevent MDR (isoniazide resistance)

Drug | Initial phase | Continuation phase
--- | --- | ---
Isoniazid | X | X
Rifampin | X | X
Ethambutol | X | -
Pyazinamide | X | -

## Cavitation
It is not unusual to have + culture at the end of the 2nd week of thearpy. TB replicates really slowly (18/20hrs) which makes it slower to disease. Cavitary disease is really difficult to treat, high relapse rate.

## Cavitation treatment
Extend treatment to 9 months (+ 7mo continuation phase)

## TB assessment/screening
At risk --> TST (skin test; Mantoux method) +/or IGRA + symptom

## Latent TB

LTBI treatment: INH
Problems while treating; lost to follow-up, poor adherence. Seems to be relationship between shorter therapy and inreased compliance.

## T-Spot. TB
Sandwich ELISA, detects cytokine released from WBC when exposed to antigen. Positive when > 8 spots are present.

## IGRAS vs. TST
TST False Postitives; I patient has received _M. bovis_ vaccine or exposed to BCG substrain.

## CDC Recommendation
IGRAs can be used in place of skin test (TST), IGRA is prefferred for populations that are unlikely to return to get TST read (24-48hrs), for testing patients that have received BCG vaccine.

## 3HP vs. 9 mo INH

## DM vs non-DM Culture Conversion
Increased risked of culture positive at the end of 2nd week with DM patients.

Can force patient to take therapy; i.e take to court, police involvement. Try to make treatment as easy as possible to patient, helps your case in court.

## HIV-TB vs DM-TB
Similar outcomes; Both increase TB cases, more difficult to diagnose TB, incrased death, increased recurrence.

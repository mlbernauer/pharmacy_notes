# Lecture I - Anesthetics, Epistaxis, and Prophylaxis

_by_ [_Michael L. Bernauer_](http://mlbernauer.bitbucket.org/)

### Opthalmic Local Anesthetics

Name | Available formulation | Dose | Freq | Druation |
--- | --- | --- | --- | --- |
Tetracaine | 0.5% Opthalmic | 1-2 gtt | Q 5-10 min | 1-3 doses (3-5 prolonged procedures)
Proparacaine | 0.5% Opthalmic | 1 -2 gtt | Q5-10 min | up to 7 doses (prolonged produces)

### Topical Preparations
Name | Available formulation | Dose | Onset | Duration
--- | --- | --- | --- | --- |
EMLA (lidocaine:prilocaine) | 2.5% : 2.5% | 25mg / 1gm | 1hr | 1-2hr
LMX4 (liposomal lidocaine) | 4% | - | 30 min | 60 min
Lidocaine J-Tip | 1% injection | 0.02 - 0.25 mL | 1-3min |  -
Cocaine | 4% topical soln (200mg) | - | - | -
TAC (tetracaine, epinephrine , cocaine) soln | 4%, 1:2000, 11.8% | 5-10mL applied to wound | 10 min | -
LET (lidocaine, epinephrine, tetracaine) | 4%, 1:2000, 0.5% | 5mL on gauze | 10-30 min | -

### Other Topical Preparations
Name | Formulation | Dose | Notes |
--- | --- | --- | --- |
Lidocaine jelly (Urojet) | 2% | 600mg max in 12hr period | Dwell time 5-10 min, instilled in urethra
Viscous lidocaine | 2% | 5-10mL (oral), 5-15mL (GI), 3-5mL (NG tube) | used in GI coctail/magic mouthwash

### Unsuspecting Local Anesthetics
Name | Formulation | Notes
--- | --- | --- |
Diphenhydramine | - | Use in pts with '-cain' allergy, causes tissue necrosis
Benzyl Alcohol | 0.9% injectable | fewer SE's than diphenhydramine

### Treatment Agents
Name  | Formulation | Dose  | Mechanism | Onset | Duration
---  | --- | ---   |  --- | --- | ---
Cocaine | 4% soln | 4ml unit dose (1-3mg/kg max or 400mg) | Vasoconstrict + block conduction of nerve impulse. | 1 min | 30-60 min
Phenylephrine | 0.25%, 0.5% nasal soln (spray/drops)| 1-2 gtt/sprays, soaked gauze| Sympathomimetic (&alpha; agonist) vasoconstrictor | - | -
Oxymetazoline | 0.05% soln | 2 sprays each nostril | Imidazoline decongestant (&alpha; agonist), vasoconstrictor | - | -
Silver nitrate | 10% soln on cotton applicator | - | Coagulates protein to form eschar | - | -
Thrombin JMI | Expistaxis Kit (syringe delivery) | 5000 IU | clots fibrinogen | - | -
Tranexamic acid | - | Injectable | antifibrinolytic | - | -

## Prophylaxis Antibiotic Recommendations:
####Eastern Association for Surgery of Trauma
Injury | Coverage | Medications | Duration
--- | --- | --- | ---
All open fractures | Gram + | Cefazolin | x24hr post wound closure
Grade III | Gram +/- | Cefazolin + Gentamicin | x72hr not more than 24hr after soft tissue coverage of wound
Barnyard | Gram +/- and Anaerobe | Cefazolin + Gentamicin + Penicillin | -

### Abdominal Pre-Op Antibiotics
Procedure | Medications
--- | ---
Biliary tract | Cefazolin, cefoxitin, cefotetan
Appendectomy (uncomplicated) | Cefoxitin, cefotetan, cefazolin + metronidazole
Colorectal | cefazolin/ceftriaxone + metro, cefoxitin, cefotetan, amp/sulbactam
PCN allergy | clindamycin/vancomycin + aminoglycoside/fluoroquinolone OR metronidazole + AMG/fluoroquinolone

### Vaccination Series
Vaccine | Dose | Indications | Schedule | Notes
--- | --- | --- | --- | ---
DT and TDaP | - | < 7 yo | 2,4,6, 15-18 mo and 4-6yrs | DT if child cant tolerate aP
Tdap | - | 11-18 yo and > 19 yo, pregnant in 3rd trimester, 7-10yo no fully immunized | - | -
Td | - | Booster for adults Q10yrs or after exposure | - | -
Immunoglobulin | Adult 250U IM | unknown immunization Hx OR < 3 booster shots | - | Can be used if pt has allergy to toxid.
Toxoid | - | Pts with more than 3 doses, clean minor wounds if > 10 yrs after vaccine, or dirty/major wounds > 5 years post vaccine | - | -

### When to give Toxoid/Immunoglobulin
Immune Hx | Clean minor wound (Td/TIG) | All other wounds (Td/TIG)
--- | --- | ---
Unknown # of doses OR incomplete vaccine Hx | Y/N  | Y/Y
Complete Hx + > 10 yrs since last update | Y/N | Y/N
Complete Hx + 5-9 yrs since last update | N/N | Y/N
Complete Hx + < 5 yrs since last update | N/N | N/N

### CDC Td Recommendations
All adults git single dose of TDaP ins place of next Td if they have not recieved TDaP in the past or status is unknown.

### Needle Size
Length | Indication
--- | ---
5/8" | Adults < 60kg
1" | Adults 60-70kg
1-1 1/2" | Female 70-90kg OR Male 70-118kg
1 1/2" | Female > 90kg OR Male > 118kg

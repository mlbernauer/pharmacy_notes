# PHRM 760 - Literature Evaluation Exercise
_by_ [_Michael L. Bernauer_](http://mlbernauer.bitbucket.org/)

02/13/2015

## Objectives
1. To read, critique and prepare a type-written critique/report of a published pharmacoeconomic research article using and established critique rubric.
2. To provide an opportunity to apply knowledge gained in course in completion of the type-written critique/response of a published pharmacoeconomic research article.
3. To practice scientific writing skills in completion of the type-written critique/report of a published pharmacoeconomic research article.

## Description
You will be required to read, critique and prepare a type-written critique/report of a published pharmacoeconomic research article using an established critique rubric discussed in class. The critique rubric to be employed is the 14-questions critique rubric as outlined and described in Chapter 3, _Critiquing Research Articles_ in Essentials of Pharmacoeconomics, 2<sup>nd</sup> Edition (2014, Lippincott Williams & Wilkins) by Karen Rascati, PhD.

Your critique/report should be well organized, clear, concise, and should coontained clearly delineated headings addressing the 14 question critique rubric. The critique/report should by type-written. There is no page limit. The assignment is due at the end of the Februrary 18, 2015 class peroid and there is no penalty or reward for early completion or submission. You may discuss the article or your answers with your group members, but this is an individual assignment and the written critique must be completed by you in your own words.

## Grading
The assignment is worth 10% of your grade. Each of the 14 items on the critique will be given equal weight (i.e. 10 points x 14 critique questions = 140 possible points). The number of points earned per critique/component will be determined by depth, analysis and completeness of answer.

## Critique

### Is the title appropriate?
The title _Cost Effectiveness of Leukotriene Receptor Antagonists versus Long-Acting Beta-2 Agonists as Add-On Therapy to Inhaled Corticosteroids for Asthma_ was clear and provided insight to the study objective. The title described what two agents
were being compared as well as the type of study being conducted.

### Is a clear objective stated?
The objective of the study was to estimate the cost effectiveness of LTRAs compared
with long-acting &beta;2-agonists as add-on therapy for patients whose asthma symptoms are not controlled on low-dose ICS. The objective statement is clear, accurately describing the patient population as well as what two agents are being compared.

### Were the appropriate alternatives or comparators considered?
The two comparators (&beta;2 agonists vs LTRAs are appropriate as the first choice for add-on therapy is a long acting &beta;2 adrenergic receptor agonist (&beta;2 agonist), followed by leukotriene receptor antagonists (LTRAs) for patients with uncontrolled asthma.

### Was a comprehensive description of the competing alternatives given?
The authors thoroughly describe the two treatment arms to which patients are randomized. Patients randomized to the LTRA arm receive montelukast 10mg once daily or zafirlukast 20mg twice daily in addition to their inhaled corticosteroid (ICS). Participants randomized to the &beta;2 agonist arm receive salmeterol on top of their ICS thearpy or a fixed-dose combination of either fluticasone-salmeterol or budesonide-formoterol in place of their ICS thearpy.

### Is the perspective of the study addressed?
The study takes the perspective of the NHS and society.

### Is the type of study stated?
The authors describe this study as a 2-year, pragmatic, randomized controlled trial designed to measure long-term effectiveness and total cost of asthma management for patients.

### Were all the important and relevant costs included?
The authors estimated cost by analyzing utilization of several resources related to asthma, respiratory tract infections or other relevant allergic conditions. Costs were computed from i) prescribed medications and devices ii) OTC medications iii) primary and secondary-care activity and iv) lost productivity. Although these costs are appropriate given the studies societal and NHS perspective, the authors did not include indirect costs related to caregivers.

### Were the important and relevant outcomes measured?
The outcomes were disease-specific health related quality of life instruments; Mini Asthma Quality of Life Questionnaire (MiniAQLQ), Asthma Control Questionnaire (ACQ) and the generic EQ-5D questionnaire. These instruments are appropriate for assessing health related quality of life measures as related to asthma.

### Was adjustment appropriate? Conducted? Discounting appropriate? Conducted?
Discounting was applied at 3.5% for the 2 years and 2 month observation period. The discounting factor used is appropriate given the duration of the study.

### Are assumptions stated and reasonable?
Prescription medicines, unit costs were indexed using the _British National Formulary_ code. OTC medications were given by patients or estimated from pharmacy websites. Indirect costs were valued by multiplying self-reported number of hours off work by a unit costs of &pound;13.13, which is the national average gross hourly wage in 2005. These assumptions used in the analysis of cost seem appropriate although it is not clear why the authors used wage data from 2005 while the study was published in 2010. Additionally, in the analysis of the study it was assumed that prescribers did not have bias towards a particular arm of therapy which may have resulted in some patients having a higher propensity to be switched away from LTRAs to &beta;2 agonists.

### Were sensitivity analyses conducted for important estimates or assumptions?
Although the authors mention that prescribe bias towards &beta;2 agonists may influence the results, no analysis was done to determine if this was the case. The authors did however run separate analysis with imputed values (Rubin's multiple imputation) and without imputation and concluded that the analysis ran with imputation resulted in better results.

### Were limitations addressed?
Limitations were addressed. The authors mention that prescriber bias towards a particular treatment may result in more patients being switch to another therapy; therefore influencing the results. Additionally, the study relied on self-reported data which is prone to several shortcomings including recall bias.

### Were extrapolations beyond the population studied proper?
The authors claim that the study was designed to generalize to the primary care setting. To achieve this the authors used relatively non-restrictive inclusion criteria to closely approximate the primary care-demographic. The study population age ranged from 16-65, therefore it would not be sensible to extrapolate these findings to include patients outside of the age ranges.

### Was an unbiased summary of the results presented?
The authors present an unbiased summary of their findings. The authors concluded that the study study findings support repositioning of LTRAs as add-on therapy to ICS however, caution that there is much uncertainty surrounding the cost effectiveness of LTRAs.

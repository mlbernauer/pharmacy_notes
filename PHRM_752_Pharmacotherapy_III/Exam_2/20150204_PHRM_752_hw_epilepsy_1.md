#PHRM-752 Exam II - HW I
_by_ [_Michael L. Bernauer_](http://mlbernauer.bitbucket.org)

2/2/2015

### Case
CC: "I have bruising spots on my arms and thighs. I feel dizzy."

HPI: LW, a 22-yo white female, started having seizures at the age of 14 after resection of glioma.

Seizure descriptions: ocur during daytime, 2-3times/wk, last 2-3 minutes, + loss of consciousness, heavy headache after episode.

PMH:
* Polycycstic ovary syndrome at the age of 20, associated with abnormal menstrual cycles.
* Vitamin D deficiency
* Vitamin level on January 5, 2014: 20ng/mL (normal range is 30~74ng/mL)

Medications:
* Valproic acid 1500mg PO BID x 10 years
* Multivitamin 1tab PO daily
* Metformin 500mg PO BID, pharmacist says patient has diabetes even though no glucose/HgA1c levels have been found.
* Patient has tried phenytoin, carbamazapine, oxcarbazepine, none of which were effective.

#### Question 1: What would you like to know more about this patient to make a care plan for the patient?

I would like to assess whether or no the patient has been compliant with her previous medications. Whether or not this patient has any drug allergies or adverse drug reactions to any of the previously tried antiepileptic drugs. Additionally, I would like to gather more information regarding the seizure type.


#### Question 2: Would you agree with the pharmacist's explanation regarding metformin?

No, metformin is commonly used to lower blood glucose levels in patients with polycystic ovary syndrome. It is likely that metformin was orginally prescribed for this purpose given her history of polycystic ovary syndrome and the long term use of valproic acid.




#### Question 3: How would you like to manage this case.
**AED management?**

From the information present the most logical assessment of the patient condition is complex partial. With this assessment the patient should discontinue valproic acid and start levetiracetam.

New therapy: Titrate

Levetiracetam 500mg PO BID x 2 weeks

**Titrate upwards as necessary every 2 weeks

**ADR management?**

The bruising is probably the result of valproic acid induced thrombocytopenia and should resolve gradually once valproiv acid is discontinued. Possible adjustment of vitamin D if vitamin levels to not begin to normalize. Additionally could consider adding folic acid.

**Necessary labs?**

CBC, blood chemistries, Vitamin D levels

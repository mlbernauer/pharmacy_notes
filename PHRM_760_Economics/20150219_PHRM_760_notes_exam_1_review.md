# PHRM 760 - Exam I Review
_by_ [_Michael L. Bernauer_](http://mlbernauer.bitbucket.org/)

## Cost Utility and Cost Benefit Analysis
### Learning objectives
1. Define _cost_ and _opportunity cost_.
2. Understand cost categorization
  * Direct medical costs
  * Direct non-medical costs
  * Indirect costs
  * Non-tangible costs
3. Methods of measuring indirect costs
  * Human capital approach
  * Willingness-to-pay (WTP) approach


### Costs
Only costs related to the services/treatment/illness should be considered.
Costs do not always occur in the form of money (i.e. time lost, etc.).
The costs of resources involved in any action or decision provide a fairly
reasonable estimate of cost. All costs matter, however some are harder to measure than others.

* ***Direct costs*** include payments made for medical services and could include direct medical costs or direct non-medical costs.
* ***Indirect costs*** include lost time and/or productivity due to an illness. Frequently referred to as productivity cost. Lost productivity may include productivity lost due to premature mortality.
* ***Intangible costs*** includes things like pain, suffering, fatigue and anxiety.

### Direct costs
Easiest to measure, include cash payments for services (i.e. actual transactions).

_Direct costs = ($ per transaction) x (# transactions per event) x (# of events)_

***Direct medical expenses*** include those related to medical services only (visits, medications, medication administration, hospitalization).

***Direct non-medical*** includes costs accrued from non-medical sources (transportation, hotel).

## Health-Related Quality of Life (HRQOL) Assessment
### Learning objectives
1. Explain the ECHO model.
2. Define HRQOL and its role in assessing the impact of therapy and/or disease
   progression.
3. Describe domains usually incorporated into HRQOL measurements.
4. Describe ceiling and floor effects in HRQOL instruments.
5. Name and explain differences between generic HRQOL instruments.
6. Explain basic concepts of administration and interpretation of HRQOL instruments.
7. Tell how disease-specific HRQOL instruments are used.
8. Explain the additional information captured by disease-specific HRQOL instruments.

# PHRM-760 Econ Exam I - Health Related Quality of Life

## Learning Objectives
1. Explain the ECHO model
 * Echo Model includes Economic, Clinical and Humanistic outcomes.
 * Clinical: cure rates, decrease in symptoms, improvement in physiologic measures.
 * Economic: cost of therapy, costs/savings of future health care expenditures, costs of adverse events, unemployment, family care givers.
 * Humanistic: health-related quality of life, disability, quality, adjusted life year.
2. Define HRQOL and its role in assessing the impact of therapy and/or disease progression.
 * WHO: Health is a state of complete physica, mental, and social well-being and not merely the absence of disease or infirmity.
3. Describe the domains usually incorporated in HRQOL measurements.
 * Domains are aggregate variables which specify a component of HR-QOL.
 * Domains usually measured.

Domain | Notes
--- | --
Physical status and functional abilities | -
General health perceptions | -
Psychological status and well-being | -
Social and role functioning | -

4. Describe ceiling and floor effects of HRQOL instruments.
5. Name and explain differences between generic HRQOL instruments.
6. Explain basic concepts of administration and interpretation of HRQOL instruments.
7. Tell how disease-specific HRQOL instruments are used.
8. Explain the additional information capture by disease-specific HRQOL instruments.

## Notes:

### Domains of HRQOL

### Types of HRQOL Measures
Type | Tools
--- | ---
General health status measures | SF-36, SF-36V, SF-12, WHO QOL, Nottingham Health Profile
Patient preference measures | Quality and Quantity of Life (QALY)
Disease-specific | Measure specific QOL aspects associated with disease

### Sickness Impact Profile
 * Domains:
  * Independence: sleep, rest, eating, work, home management, recreation, pasttimes (43 items)
  * Physical: ambulation, mobility, bodily care and movement
  * Emotional: social interaction, alertness behavior, emotional behavior, communication (48item)

### Nottingham Health Profile - NHP
Emotional, social, and physical health problems.
Subjective assessment of health status.

### General
These tools are really subjective and should only be used to compare between groups, or to measure the same individual over time.

### Scaling Issues
Use ordinal system, however this has scalling issues. Is magnitude of change betweeen response 2 and 3 the same as the change between responses between 3 and 4.b

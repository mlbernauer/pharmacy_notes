# PHRM 760 - Critiquing Economic Effectiveness Research Articles
_by_ [_Michael L. Bernauer_](http://mlbernauer.bitbucket.org/)

02/14/2015

### Learning Objective
1. Know and understand critical elements for an economic effectiveness study.

## Rascati Guidelines to Assessing Credibility
1. Is the title appropriate?
2. Is a clear objective stated?
3. Were the appropriate alternatives or comparators considered?
4. Was a comprehensive description of the competing alternatives given?
5. Is the perspective of the study addressed?
6. Is the type of study stated?
7. Were all the important and relavent costs included?
8. Were the important or relevant outcomes measured?
9. Was adjustment appropriate? Conducted? Discounting appropriate? Conducted?
10. Are assumptions stated and reasonable?
11. Were sensitivity analyses conducted for important estimates or assumptions?
12. Were limitations addressed?
13. Were exploitations beyond the population studied proper?
14. Was an unbiased summary of the results presented?


### Is the title appropriate?
The title should provide information as to what is being compared, what type of economic analysis is being conducted and should be objective.

### Is a clear objective stated?
The authors should clearly describe the objective of the study.

For example _"To examine whether Hib immunization should be included in a universal immunization program in Korea, an economic evaluation of Hib immunization was conducted with societal perspective._
"

### Were the appropriate alternatives or comparators considered?
Was comparison made with standard of care, are the alternatives used appropriate for the
study? If outdated treatment/service is used as alternative, compelling evidence must be
provided to support its use.

### Was a comprehensive description of the competing alternatives given?
Can comparators be replicated, enought detail provided, treatment dosage and frequency.

### Is the perspective of the study addressed?
Insurer, society, clinic, patient, facility

### Is the type of study stated?
Authors should specifically state the purpose of the study; Cost effectiveness (life years, avoided events etc)
Cost-utility, Cost benefit, Cost minimization.

### Were all the important and relevant costs included?
Depends on the reviewer and the perspective given. Different perspectives may
require different cost considerations. Costs used should be given and appropriate to
the study.

### Were all important or relevant outcomes measured?
Depends on the reviewer and perspective. Different perspectives may require
different outcome measurements.

### Was adjustment appropriate? Conducted? Discounting appropriate? Conducted?
Cost adjustment included standardizing monetary units to same year, particularly
important with retrospective studies. Appropriate discounting to factor in time-value
of money.

### Were assumptions stated and reasonable?
Study may need to rely on other studies/information for elements used in analysis.

### Were sensitivity analyses conducted for important estimates or assumptions?
Changes in some factors may alter findings. Were these considered? Were estimates
calculated for variations in assumptions?

### Were limitations addressed?
Omissions, failure to generalize, failure to reject null hypothesis, sample size
limitations.

### Were extrapolations beyond study population proper?
Primarily deals with ability of study to generalize to other populations.
Authors should not generalize to populations that are not congruent to study
population.

### Was an unbiased summary of the results presented?
Are the authors' conclusions supported by the evidence from the study? Do findings
make sense. Were there potentially adverse effects from unmeasured confounders?
Were any conflicts of interest addressed?

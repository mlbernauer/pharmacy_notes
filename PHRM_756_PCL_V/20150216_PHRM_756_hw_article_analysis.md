# PHRM 756 - Article Analysis
_by_ [_Michael L. Bernauer_](http://mlbernauer.bitbucket.org/)

02/16/2015

## Long-term safety and sustained efficacy of extended-release pramipexole in early and advanced Parkinson's disease

### Title:
Long-term safety and sustained efficacy of extended-release pramipexole in
early and advanced Parkinson's disease

### Author:
R.A. Hauser, A. H. V. Schapira, P. Barone, Y. Mizuno, O. Rascol, M. Busse,
C. Debieuvre, M. Fraessdorf, W. Poewe
## Methods:

#### Study Design
Open-label (OL) extension following double-blind (DB), randomized, placebo-controlled clinical trial.

#### Endpoints
* Safety endpoints were assessed using self-reported adverse events (AEs) and withdrawals due to AEs.
* Impulse control disorderer endpoint was assessed using modified Minnesota Impulsive Disorders Interview (mMIDI)
* Daytime somnolence was assessed using the Epworth Sleepiness Scale (ESS).
* Efficacy was assessed using parts II and III of the Unified Parkinson's Disease Rating Scale (UPDRS).

_Note: No primary/secondary designations._

#### Study population
Patients with both early and advanced Parkinson's disease enrolled in previous DB trial in which they were randomized to pramipezole ER q.d., pramipexole IR t.i.d. or placebo.

##### Inclusion criteria
* Patients with PD at Hoehn and Yahr stage 1-3
* Diagnosis within previous 5 yrs at age &ge;30
* PD at Hoehn andYahr stage 2-4 during 'on' time (advanced PD arm)
* PD diagnosed &ge;2 years before entry and were being treated with levadopa (plus dopa-decarboxylase inhibitor) at optimized dosage &ge;4 weeks. (advanced PD arm)
* Motor fluctuations &ge;2 h of daily 'off' time (advanced PD arm)

##### Exclusion criteria
* Previous levodopa exposure of > 3 months or < 3 months &le; 8 weeks prior to randomizations.
* Previous dopamine-agonist exposure < 4 weeks prior to randomization.
* Dopamine agonist &le;4 weeks (advanced PD arm)
* Secondary or atypical parkinsonism or medical/psychiatric conditions capable of impeding the patient's participation (all OL participants)

#### Statistics
Because extension studies had no randomized groups, no sample-size need was calculated. Mean changes in UPDRS II + III score were adjusted by analysis of covariance (ANCOVA), with country and DB treatment as fixed effects and DB baseline as covariate.

## Results
#### Participant flow
![](20150216_PHRM_756_hw_participant_flow.png)


Results | Early PD (33wk) | Early PD (11-13wk) | Advanced PD (33wk)
--- | --- | --- | ---
Class | I, II, III, IV | - | -
Blind | No | No | No
Power analysis | No | No | No
Primary outcome | - | - | -
Sample size (init) | 368 | 143 | 391
Sample size (final) | 291 | 117 | 329
Completion rate (%) | 79% | 82% | 84%
Intervention/Duration | OL ER pramipexole/74 week (113wk total) | OL ER pramipexole/72 week (83-85wk total) | OL ER pramipexole/74 week (113wk total)


##### Outcomes
###### Adverse Events
AE | Early PD | Advnaced PD
--- | --- | ---
Any | 82.2% | 82.9%
Mild | 36.2% | 43.7%
Moderate | 36.6% | 27.1%
Severe | 9.6% | 12.0%
Drug related AE | 39.3% | 48.8%
Resulting in DC | 9.8% | 7.9%
Death | 1.6% | 1.0%
Drug related death | 0% | 0%

###### Impulse control (mMIDI)
 mMIDI | Early PD | Advanced PD
--- | --- | ---
Abnormal behavior | 4 (0.8%) | 9 (2.4%)
Compulsive buying | 3 | 3
Pathological gambling | 1 | 2
Compulsive sexual behavior | 0 | 3
Compulsive buying/sexual behavior | 0 | 1

###### Daytime somnolence (ESS)
 ESS | Early PD | Advanced PD
 --- | --- | ---
 Score > 10 (baseline) | 21.2 % | 26.6%
 Score > 10 (end) | 24.2% | 33.7%

###### Sustained efficacy (part II+III UPDRS)
![](20150216_PHRM_756_hw_updrs_results.png)


#### Secondary and other outcome analysis
Not conducted

#### Harms during study
See AE outcomes, no deaths were attributed to study drug.

## Discussion

#### Study limitations
Open label design and recruitment of the subjects from prior trials may be considered limitations. Patients from prior study, relatively intolerant to study drug and those with small subjective improvement may have dropped out or became disinclined to enroll in further trial. Also, recruitment from previous trial may have reduced incidence of certain AEs, especially those that are likely to occur early in treatment.

#### Generealizability
Because of the studies inclusion/exclusion criteria, results from this study likely do not generalize well into demographics not included in the analysis. This includes subjects with PD not at Hoehn and Yahr stage 1-3, Diagnosed at age < 30 years old, patients with motor fluctuations &le; 2 h of daily 'off' time and those on dopamine agonists > 4 weeks.

#### Interpretation
Results from OL extension study support long-term safety and efficacy of pramipexole. AEs reported in early and advanced PD patients were typical of those for use of dopaminergic meications in aging PD population with somnolence and peripheral edema most frequent in early PD and dyskinesia and somnolence most frequent in advanced PD.

UPDRS II and III scores remained substantially improved after 80 weeks of OL treatment in both early and advanced PD as compared with DB baseline. UPDRS scores after 72-80 weeks of OL therapy were unchanged to mildly worse compared with OL baseline, as expected with disease progression.

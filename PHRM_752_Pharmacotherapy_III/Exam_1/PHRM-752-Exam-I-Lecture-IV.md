#Lecture IV - Poison Prevention and Antidotes
_by_ [_Michael L. Bernauer_](http://mlbernauer.bitbucket.org/)

## Learning Objectives
1. Understand the Poison Prevention Act and the pharmacist's role.

  * Must keep 80% of children < 5 out of product for 10 minutes while allowing 90% of adults (50-70yrs) in to the product in 10 minutes
  * Pharmacists must adhere to the Poison Prevention Packaging Act.
  * Teach patients who insist on non-compliant packaging to store medications out of reach of children.
  * Teach parents how to care for child-resistant containers.
  * Counsel patients on poison prevention when dispensing hazardous medications.

2. Match antidotes to their poison.
3. Know the indications and side effects of antidotes in this presentation.
4. Know initial dosing for antidotes in this presentation.
5. Know how to calculate osmolal gap and use it to predict need for fomepizole therapy.
6. Know the 4 hour toxic APAP level and half-life of the treatment nomogram.

## Antidotes for Common Poisons
Poison | Antidote
--- | ---
Rattlesnake evenomations | Antivenin (Fab Fragments)
Black widow envenomations | Antivenin, Lactrodectus mactans
Coral snake envenomations | Antivenin, Micrurus fulvius
Cholinesterase inhibiteors | Atropine sulfate
Botulism | Botulinum antitoxin (ABE-trivalent)
Lead | Ca<sup>2+</sup> disodium EDTA
Ca<sup>2+</sup> channel blocker | CaCl
Hydrofluoric acid | Ca<sup>2+</sup>-gluconate
Cyanide | Hydroxocobalamin
Iron | Deferoxamine (Desferal)
Digoxin/Digitoxin | Digoxin immune Fab (Digibind)
Pb, Hg, As | Dimercaparol
Antipsychoitc induced dystonia | Diphenhydramine
Plutonium, americium, curium | DPTA
MeOH, Ethylene glycol | EtOH, Fomepizole, Folic acid
Benzodiazepines | Flumazenil (Romazicon)
&beta;-blocker, Ca<sup>2+</sup>-blocker, OHAs | Glucagon
&beta;-blocker, Ca<sup>2+</sup>-blocker | Insulin + Glucose
Local anesthetic | Intralipids
Methemoglobinemia | Methylene blue
Acetaminophen | N-acetylcysteine (Mucomyst, Acetadote)
Opioids | Naloxone (Narcan)
Tubocurarine | Neostigmine
Sulfonylurea | Octreotide + Glucose
Carbon monoxide | Oxygen
Heavy metals | Penicillamine
Anticholinergics | Physostigmine
Oral anticoagulants | Phytonadione
Choninesterase inhibiters | Pralidoxime
Cs<sup>137</sup>, thalium | Prussian blue
Isoniazide, monomethylhydrazine | Pyridoxine
Tricyclic antidepressants | Na<sup>+</sup> bicarbonate
Lead | Succimer

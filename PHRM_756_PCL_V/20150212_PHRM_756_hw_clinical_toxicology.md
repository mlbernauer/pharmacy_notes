# PHRM 756 - Clinical Toxicology
_by_ [_Michael L. Bernauer_](http://mlbernauer.bitbucket.org/)

02/13/2015

## Case I
A 15 year-old, 97-pound girl ingested a handful of promethazine 50 mg tablets (100 tablets = 5,000 mg) as suicide gesture. The patient presents to the emergency room less than 30 minutes later with no symptoms other than tachycardia. She is lucid and has an intact gag reflex. No treatments were instituted at home and this is the only medication involved with the exposure.

Vital signs: BP 115/65, HR 103-110, RR 22, T 98.6<sup>o</sup>F

QRS 76 sec

QTc 440 msec

#### Question 1
***What are the signs and symptoms of a promethazine overdose? Where did you find that information?***

>Clinical Presentation: antihistamine overdose results in symptoms similar to anticholinergic poisoning and includes drowsiness, dilated pupils, flushed dry skin, fever, tachycardia, delirium, hallucinations, and myclonic or choreoathetoid movements. Convulsion, rhabdomyolysis, and hyperthermia may occur with serois overdoses. Significant overdoses can result in QT interval prolongation and torsades-type atypical ventricular tachycardia.

Source: _Olson, K., Anderson, I., Benowitz, P., Poisoning and Drug Overdose 4<sup>th</sup> edition; McGrawhill_

#### Question 2
***Is she a candidate for receiving activated charcoal? Why and why not?***

>Yes, activated charcoal is indicated for reversal of antihistamine toxicity in hospitalized patients. Additionally, the patient has an intact gag-reflex and it has been less than 30 minutes since the promethazine was ingested suggesting that charcoal may be effective in reducing absorption.

#### Question 3
***If this patient is a candidate for activated charcoal, what dose should she be given? What is the best preparation to use (powdered? pre-mixed? [aqueous, sorbitol])?***

_Dose of activated charcoal: the ratio of 10:1 or 1g/kg (whichever is greater) 97lb == 44kg_

>Assuming that the patient ingested 100 tablets we can conclude the total amount of promethazine ingested is 5g. Using the 10:1 regimen we would administer 50g of activated charcoal. Studies shave suggested that gestrointestinal side effects are greater in preparations where sorbitol is used, therefore it is advised that the aqueous formulation be administered.

## Case II
A 46 year-old, 50 kg woman with a past medical history of manic depressive disease intentionally ingests 50 prenatal iron tablets (325mg FeSO<sub>4</sub>; 65 mg elemental iron per tablet).

HR: 80; BP: 120/70, RR: 18, T: 98.6<sup>o</sup>F

No treatments have been instituted at home or in the hospital.

#### Question 1
***What dose of elemental iron did she ingest?***

>(65mg FeSO<sub>4</sub>) x (50 tablets) = **3,250mg Fe**

>(3,250mg Fe) / (50kg) = **65 mg/kg**

#### Question 2
***According to the practice guideline posted in Moodle (Manoguerra eta al., Clinical Toxicology 43:553-70, 2005), is she supposed to be observed at home or evaluated and treated by a healthcare provider, according to the iron ingested (mg/kg)? Why?***

>According to the above guidelines, patients who have ingested less than 40mg/kg of elemental iron and who are having mild symptoms can be observed at home. Mild symptoms such as these occur frequently and do not necessarily prompt referral to a healthcare facility. Patients with more serious symptoms, such as persistent vomiting and diarrhea, alterations in level of consciousness, hematemesis, and bloody diarrhea, require referral.

The patient is taken to the emergency room with 30 minutes of ingestion. She is remorseful but otherwise asymptomatic.

#### Question 3
***Assuming that the dose ingested is potentially lethal, there are no contraindications to the procedure, and that the tablets do not fit through the vents in the orogastric lavage tube, what would be the best method to prevent absorption of iron and why?***

>For seriously intoxicated patients (shock, severe acidosis, serum Fe levels 500-600mcg/dL) dexferoxamine should be administered. Intravenous route is preferred and should be given 10-15 mg/kg/h by continuous infusion. The recommended maximum daily dose is 6g.  Monitor for pink urine color resulting from chelated deferoxamine-iron complex. Therapy may be stopped once urine color returns to normal.

>Whole bowel irrigation is considered first-line treatment, especially if large numbers of tablets are visible on plain abdominal x-ray.

>Avoid activated charcoal as iron has poor adsorption.

#### Question 4
***Write the order for this patient, including the endpoint of therapy.***

>1. Polyethylene glycol electrolyte solution 1500 ml/hr until rectal effluent is clear
>2. Dexferoxamine IV 13 mg/kg/h until urine color returns to normal.

#### Question 5
***How much of the treatment drug will be required?***

>650mg/h up to a maximum of 6g total.

## Case III
You are the new ED pharmacist at UNMH. At 6:30 p.m., a mother brings her teenage daughter to the ED. She reports that her daughter has been depressed for the past couple of weeks due to being dumped by the "hottest guy in school". The mother then tells you she caught her daughter swallowing an entire bottle of Tylenol. The ED attending tells you that the girl needs N-acetylcysteine immediately. However, the nurse who usually mixes it is out sick. He now asks you to mix it up because "you PharmDs are taught that kind of thing". The patient weights 50 kg. The doctor would like to give her an oral loading dose of 140 mg/kg. He wants you to mix a palatable 5% solution for the patient. Available is an N-acetylcysteine 20% solution.

Your job is to perform all necessary calculations to determine the volumes of drug and diluent that need to be mixed. Please show all work.

>##### Total N-acetylcystein desired
>(50kg) x (140mg/kg) = 7,000mg

>#### Total volume of 5% solution required for 7,000mg N-Acetylcysteine
>V = (100mL x 7g) / (5g)

>V = 140mL

>#### Volume of 20% solution required to produce 140mL of 5% solution
>Equation: M<sub>1</sub>V<sub>1</sub> = M<sub>2</sub>V<sub>2</sub>

>V = (140mL x 5%) / (20%)

>V = 35mL

>#### Preparation instructions
>Dilute 35mL of 20% N-Acetylcysteine solution with 105mL of diet soda or water to obtain 140mL of 5% solution containing a total of 140mg of N-Acetylcysteine.

## Case IV
You are a student in PCL and are going to be mixing and tasting different flavored N-acetylcysteine solutions. Your professor instructs you to make 250 mL of 5% solution so you and your classmates can have a taste. You have 20% N-acetylcystenine and a diluent.

Your task is to perform all necessary calculations to determine the volume of drug and diluent that need to be mixed. Please show all work.

>##### Volume of 20% solution required to make 250mL of 5% solution
Equation: M<sub>1</sub>V<sub>1</sub> = M<sub>2</sub>V<sub>2</sub>

>V = (250mL x 5%) / (20%)

>V = 62.5mL

>##### Preparation instructions
>Dilute 62.5mL of 20% N-Acetylcysteine with 187.5mL of diluent to obtain 250mL of 5% N-Acetylcysteine solution.

# PHRM 788 Homework I - Gathering and Interpreting Public Health Data
_by_ [_Michael L. Bernauer_](http://mlbernauer.bitbucket.org)

#### Grading Rubric
Criteria (Each response's point value is scored based upon the following percentages) | Missing | Below Average | Average | Good-Excellent
--- | --- | --- | --- | ---
All questions answered, as specified |  | | |
Responses were written clearly and properly structured | | | |
No errors in spelling, grammar & use of English | | | |
Answers are accurate | | | |

#### Instructions
1. Go to [http://www.nlm.nih./gov/nichsr/usestats/index.html](http://www.nlm.nih.gov/nichsr/usestats/index.html) adn review the course **About Health Statistics** whic is the first part of the course. Read through the topics: Importance, Use and Sources. This provides background information about health statistics.
2. Go to [http://www.cdc.gov/datastatistics/](http://www.cdc.gov/datastatistics/) and complete the exercise. **You have been assigned a topic area for questions 1, 2, 3, and 4, listed in a separate 'attachment' for this assignment**. Note: several of the conditions may link to another source of information.

#### Assignment
##### 1. Describe (a) the public health concern associated with your assigned topic area and how it impacts population health and (b) list two measures of prevalence for that condition. (5 points)
_a) Chronic Kidney Disease (CKD) is a condition in which your kidneys are damaged and cannot filter blood as well as healthy kidneys. Because of this, waste from the blood remains in the body and may cause other health problems. If left untreated, CKD can progress to kidney failure (also known as end-stage renal disease) and early cardiovascular death. More than 20 million US adults are estimated to have CKD and most are undiagnosed. Kidney disease is the 9th leading cause of death in the United States. In the United States, diabetes and hypertension are the leading causes of kidney failure, accounting for 72% or new cases. The number of CKD cases in the US population has more than tripled since 1990 and is expected to grow becaues of an aging population and the increasing number of people with conditions, such as diabetes and high blood pressure, which place them at risk of developing CKD. Total Medicare spending for patients with kidney failure reached nearly $29 billion in 2012, accounting for about 6% of the Medicare budget costs. In addition, overall Medicare costs for people aged 65 years or older with CKD were about $45 billion in 2012, or more than $20,000 per person year. [source](http://www.cdc.gov/diabetes/projects/pdfs/ckd_summary.pdf)_

_b) List two measures of prevalence for Kidney Disease._

1. [Prevalence of CKD Stages, 1988-1994 vs. 1999-2006 vs. 2007-2012 by Stage and Year](http://nccd.cdc.gov/CKD/detail.aspx?QNum=Q8)
2. [Prevalence of CKD Stages 1-4, by Survey Period](http://nccd.cdc.gov/CKD/detail.aspx?QNum=Q372)


##### 2. Answer the following questions (25 points). Health topic assigned: _Kidney Disease_

a) What is the US prevalence rate in two most recent years?

Year | State 1 | Stage 2 | Stage 3 | Stage 4 | Total
--- | --- | --- | --- | --- | ---
2011-2012 | 3.81% (CI 2.92-4.71) | 3.33% (CI 2.80-3.87) | 7.76% (CI 6.55-9.16) | 0.46% (CI 0.29-0.73) | 15.35% (CI 13.75-16.98)
2009-2010 | 2.62% (CI 2.09-3.16) | 3.50% (CI 2.73-4.27) | 7.34% (CI 6.41-8.39) | 0.47% (CI 0.30-0.72) | 13.92% (CI 12.60-15.25)
[source](http://nccd.cdc.gov/CKD/detail.aspx?Qnum=Q372)

b) What is New Mexico's rate in those years?

Year | ESRD Rate (per million) | ESRD Rate (%)
--- | --- | ---
2011 | 2020.10 | 0.202%
2010 | 2076.51 | 0.207%

c) In your own words, describe trends over time for the US and New Mexico and tell why the trends emphasize or lessen it as a public health concern.

_While the total prevalence of CKD appears to increase from the years 2009-2010 to 2011-2012, there appears to be a decrease in the prevalence of Stage II CKD. The only state level measure that was available for New Mexico is ESRD. Prevalence of ESRD rate appears to have decreased slightly from 2076.51 per million in 2010 to 2012.10 per million in 2012, however we are unable to tell if this difference is statistically significant as no confidence intervals were provided._

d) Describe an intervention that could help alleviate the problem.

_It has been established that hypertension and uncontrolled diabetes are the leading cause of CKD in the United States. Interventions focusing on diabetes and hypertension should function to reduce new cases of CKD. Pharmacologic interventions may focus on tight therapeutic management to gain control of disease state; this include dose/therapy optimization and enforcement of medication adherence. Non-pharmacological interventions should focus on behavioral and lifestyle modifications. Examples include diet and exercise programs and incentives. Rewards programs may enforce healthy behaviors. Encouraging patients to keep a close eye on their health may encourage participation and adherence to healthy lifestyle. This may include provision and use of self monitoring tools such as; blood glucose monitors and pedometers._

##### 3. Use the WHO table "GHE DALY 2012 country.xlsx" provided with this assignment and search the public health topic you were assigned. _If the conidtion you were assigned is not shown, select another public health concern. (20 points)_

a) What is the US rate for the health concern regarding:

Country | Disability-adjusted life years (DALYs)
--- | ---
United States  | 1443.1

b) Identify a developing country and obtain the country's rate for the health concern.

Country | Disability-adjusted life years (DALYs)
--- | ---
Sierra Leone  | 59.7

c) In your own words, define DALYs and describe how your health concern impacts DALYs.

_Disability Adjusted Life Years attempts to measure the total burden of disease which includes the number of life years lost as a result of having the disease as well as the total number of years spend disabled. This measure is an adjusted version of the "total life years lost" measure that includes those years lived by the patient while suffering from the disease._

d) Provide 3 reasons why DALYs for a health condition vary between different countries, especially between the US and lower income (developing) countries?

_The DALYs reported by developing countries may give a false sense of overall health. For example, Sierra Leone has a Kidney Disease DALY reported value of 59.7 years while the United States has a reported DALY of 1443.1. At first glance, this falsely gives the impression that Sierra Leone has superior health status compared to the United States. However, it is my belief that the discrepancy in DALY measurements reported by the two countries is confounded by the fact that in the United States, patients tend to live longer with the disease as a result of better medical treatment. This causes the number of 'years disabled' to be disproportionately high compared to those individuals suffering from kidney disease in third world countries. Another explanation could be that because CKD is the result of chronic conditions (i.e. hypertension, and diabetes) only individuals who live long enough for these conditions manifest and progress to CKD are affected. If individuals in Sierra Leone have shortened life expectancy or expire as a result of diabetes or hypertension associated complications (other than CKD) then they will never develop CKD which results in lower reported DALY for this disease._

e) Review the excel file labeled "WHO leading cause of Death.xlsx" and answer the following questions.

1. Comparing low income with high income nations, what are 3 differences?

  * _Individuals in lower-income countries tend to be suffering from infectious disease processes while those in higher income countries suffer from cardiovascular disease._
2. What are 2 implications for differences in governmental public health programs between the 2 types of countries?
  * _Public health programs of low-income countries likely include vaccination programs, sanitation, and transmission related education programs.
  * _ Public health programs of high-income countries likely focus on prevention of chronic cardiovascular related complications as well as behavior and lifestyle modifications._

3. Comparing global 2000 versus 2012 data, what changes have occurred? Why?
  * _More people appear to be dying of trachea, bronchus, and lung cancers in 2012 compared to 2000. This may be due to advanced lung disease resulting from the pre-antismoking campaign demographic. For example, lung cancer manifesting in long term smokers. Additionally, hypertensive heart disease and diabetes mellitus seem to have moved up since 2000. This may related to increased rates of obesity and increasingly sedentary lifestyles._

##### 4. Go to th NM department of Health indicator database web site [http://ibis.health.state.nm.us/indicator/index/Alphabetical.html](http://ibis.health.state.nm.us/indicator/index/Alphabetical.html). If the condition you are assigned is not shown, select another public health concern. (20 points)

**_Kidney Disease_ not shown, using _Asthma_**

a) State the statistics you choose to review:
  * _Asthma: Emergency Department Visits-Crude Rates by County_ [source](https://ibis.health.state.nm.us/indicator/view/AsthmaED.Rate.Cnty.html)

b) What 2 counties within the state are the highest rates (worst health) and what are the rates?

County | Statistic | Rates (per 10,000)
--- | --- | ---
Quay  | ED Visits | 92.95
Sierrya | ED Visits | 81.19

c) What 2 counties within the state are the lowest (best health) and what are their rates?

County | Statistic | Rate (per 10,000)
--- | --- | ---
Harding | ED Visits | 7.21
Catron | ED Visits | 14.71

##### 5. Identify 2 factors that are most modifiable through a public health intervention for this health concern. (5 points)

_a) Asthma Self-Management Education Among Youths and Adults -- Provide additional information and training which would include techniques for identifying triggers, allergen avoidance, proper medication usage and techniques._

_b) Ensuring patients are on appropriate and optimal therapy for their disease. Education to include proper use of rescue and maintenance inhalers as well as premedication techniques for imminent/anticipated attacks._
